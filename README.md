
[![No Maintenance Intended](https://img.shields.io/badge/deprecated-No_maintenance_intended-red)]([http://unmaintained.tech/](https://img.shields.io/badge/deprecated-No_maintenance_intended-red)) 

This is no longer supported, please consider using [this](https://docs.cashfree.com/docs/android-integration) instead. New merchants will not able able to collect payments using this SDK.

# Cashfree SDK Integration Steps

### Please refer our official android documentation [here](https://dev.cashfree.com/payment-gateway/integrations/mobile-integration/android-sdk).

### Discord
Join our [discord server](https://discord.gg/znT6X45qDS) to instantly get connected
