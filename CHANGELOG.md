# Changelog
Release notes for last 5 releases will be documented in this file.

## [1.7.26]
- Fixed IllegalArgumentException in NonWebBaseActivity
- Retry strategy mofdified for CFAnalytics
- Implemented batch processing for CFAnalytics


## [1.7.25]
- FIX AnalyticsExceptionHandler

## [1.7.24]
- Fixed java.lang.NullPointerException on CFNonWebBaseActivity.class
- Fixed Dialog Leak in CFBasePaymentActivity.class
- Added new crash logging module dependency

## [1.7.21] - 2022-04-18

### Changed
- Crash fixes in Web Checkout flow.

## [1.7.20] - 2022-03-28

### Changed
- NPE fix in Crash logging module.

## [1.7.19] - 2022-04-25

### Changed
- Crash fix in Web Checkout flow.
- Changed the default theme colors to Cashfree's new colors.
- Android intent vulnerability fix.

## [1.7.17] - 2022-02-23

### Changed
- Optional switching to GooglePay in-app flow inside UPI Intent flow by using "allGPayInapp" flag in input params. 

## [1.7.15] - 2021-12-22

### Changed
- Paypal web page back navigation fix.
